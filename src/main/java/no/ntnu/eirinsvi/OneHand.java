package no.ntnu.eirinsvi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.graalvm.compiler.debug.TTY.out;
import static sun.net.InetAddressCachePolicy.get;

public class OneHand {
    private ArrayList<PlayingCard> oneHand;

    public OneHand(){
        oneHand = new ArrayList<>();
    }

    public void addPlayingCard(PlayingCard playingcard){
        oneHand.add(playingcard);
    }

    public ArrayList<PlayingCard> getOneHand(){
        return oneHand;
    }

    public int sumOfHand(){
        return oneHand.stream().map(PlayingCard::getFace).reduce((a,b) ‐> a+b);
    }

    public String getHearts(){
        List<String> hearts = oneHand.stream().filter(p -> p.getSuit() == 'H')
                .map(PlayingCard::getAsString)
                .collect(Collectors.toList());
        if(hearts.isEmpty()){
            return "No Hearts";
        } else{
            hearts.forEach(s -> return s);
        }
    }


    public boolean searchForSpadeQueen(){
        return oneHand.stream().anyMatch(p -> p.getAsString().equals("S12"));
    }

    public boolean flush(){
        //oneHand.stream().map(PlayingCard::getSuit).allMatch(oneHand.get(0)::equals);
        return oneHand.stream().map(PlayingCard::getSuit).distinct().count() <=1;
    }
}
