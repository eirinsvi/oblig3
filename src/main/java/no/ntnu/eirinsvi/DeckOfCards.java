package no.ntnu.eirinsvi;

import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {
    Random random= new Random();
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private ArrayList<PlayingCard> deckOfCards = new ArrayList<>();

    public DeckOfCards(){
        for(int i=1; i<14; i++){
            for(int j=0; j<4; j++)
                deckOfCards.add(new PlayingCard(suit[j], i));
        }
    }
    public ArrayList<PlayingCard> dealHand(int n) throws IllegalArgumentException{
        if(n>0){
            ArrayList<PlayingCard> clone = (ArrayList<PlayingCard>) deckOfCards.clone();
             for(int i = 0; i<=n; i++){
                 new OneHand().addPlayingCard(clone.get(random.nextInt(52)));
                 clone.remove(clone.get(random.nextInt(52)));
            }
        return new OneHand().getOneHand();
        } else{
            throw new IllegalArgumentException("You must deal at least one card.");
        }
    }
}
