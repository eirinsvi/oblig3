package no.ntnu.eirinsvi;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {
        var javaVersion = SystemInfo.javaVersion();
        var javafxVersion = SystemInfo.javafxVersion();

        GridPane rootNode = new GridPane();
        rootNode.add(new Button("Deal hand"), 5, 3);
        rootNode.add(new Button("Check hand"), 5, 5);
        rootNode.add(new Label("Sum of the faces:"), 2, 10);
        rootNode.add(new Label("Cards of hearts:"), 2, 11);
        rootNode.add(new Label("Flush?"), 2, 12);
        rootNode.add(new Label("Queen of spades?"), 2, 13);

        stage.setScene(new Scene(rootNode, 300, 250));
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}